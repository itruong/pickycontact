package com.timka.contacteco;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Message
{
	public String id;

	public String fromuserid;

	public String created;

	public String data;
}
