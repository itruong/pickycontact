package com.timka.contacteco;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class User
{
	public String id;
	
	public String login;
	
	public String password;
	
	public String created;
	
	public String[] friends;
	
	public String[] friendrequests;
}
