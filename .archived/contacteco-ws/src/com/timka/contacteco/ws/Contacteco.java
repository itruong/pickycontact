package com.timka.contacteco.ws;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.codehaus.jettison.json.JSONArray;

import com.timka.contacteco.DataManager;
import com.timka.contacteco.Message;
import com.timka.contacteco.User;

@Path("/contacteco")
public class Contacteco implements IContacteco
{
	@GET
	@Path("/userexists/{login}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.TEXT_PLAIN)
	@Override
	public String userExists(@PathParam("login") String login)
	{
		String id = DataManager.getInstance().userExists(login);
		return (id != null) ? id : "false";
	}

	@GET
	@Path("/getuser/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Override
	public User getUser(@PathParam("id") String id)
	{
		return DataManager.getInstance().getUser(id);
	}

	@GET
	@Path("/createuser/{login}/{pass}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.TEXT_PLAIN)
	@Override
	public String createUser(@PathParam("login") String login, @PathParam("pass") String pass)
	{
		String id = DataManager.getInstance().createUser(login, pass);
		return (id != null) ? id : "false";
	}
	
	@GET
	@Path("/deleteuser/{id}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.TEXT_PLAIN)
	@Override
	public String deleteUser(@PathParam("id") String id)
	{
		return String.valueOf(DataManager.getInstance().deleteUser(id));
	}

	@GET
	@Path("/createmessage/{from}/{to}/{msg}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.TEXT_PLAIN)
	@Override
	public String createMessage(@PathParam("from") String fromuserid, @PathParam("to") String touserid, @PathParam("msg") String message)
	{
		return DataManager.getInstance().createMessage(fromuserid, touserid, message);
	}

	@GET
	@Path("/createfriendrequest/{from}/{to}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.TEXT_PLAIN)
	@Override
	public String createFriendRequest(@PathParam("from") String fromuserid, @PathParam("to") String touserid)
	{
		return String.valueOf(DataManager.getInstance().createFriendRequest(fromuserid, touserid));
	}

	@GET
	@Path("/getmessages/{to}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Override
	public Message[] getMessages(@PathParam("to") String touserid)
	{
		return DataManager.getInstance().getMessages(touserid);
	}

	@POST
	@Path("/acceptmessages")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.TEXT_PLAIN)
	@Override
	public String acceptMessages(JSONArray messageids)
	{
		DataManager.getInstance().acceptMessages(toStringArray(messageids));
		return "true";
	}

	@POST
	@Path("/acceptfriendrequests/{touserid}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.TEXT_PLAIN)
	@Override
	public String acceptFriendRequests(JSONArray fromuserids, @PathParam("touserid") String touserid)
	{
		DataManager.getInstance().acceptFriendRequests(toStringArray(fromuserids), touserid);
		return "true";
	}

	@POST
	@Path("/declinefriendrequests/{touserid}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.TEXT_PLAIN)
	@Override
	public String declineFriendRequests(JSONArray fromuserids, @PathParam("touserid") String touserid)
	{
		DataManager.getInstance().declineFriendRequests(toStringArray(fromuserids), touserid);
		return "true";
	}
	
	private String[] toStringArray(JSONArray array)
	{
		String[] result = new String[array.length()];
		for (int i = 0; i < array.length(); i++)
		{
			try
			{
				result[i] = array.getString(i);
			}
			catch (Exception x)
			{
				result[i] = "Error: not parsed";
			}
		}
		return result;
	}
}
