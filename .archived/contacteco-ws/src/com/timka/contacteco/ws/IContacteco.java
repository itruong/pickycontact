package com.timka.contacteco.ws;

import org.codehaus.jettison.json.JSONArray;

import com.timka.contacteco.User;
import com.timka.contacteco.Message;

public interface IContacteco
{
	String userExists(String login);
	
	User getUser(String id);
	
	String createUser(String login, String pass);
	
	String deleteUser(String id);
	
	String createMessage(String fromuserid, String touserid, String message);
	
	String createFriendRequest(String fromuserid, String touserid);
	
	Message[] getMessages(String touserid);
	
	String acceptMessages(JSONArray messageids);
	
	String acceptFriendRequests(JSONArray fromuserids, String touserid);
	
	String declineFriendRequests(JSONArray fromuserids, String touserid);
}
