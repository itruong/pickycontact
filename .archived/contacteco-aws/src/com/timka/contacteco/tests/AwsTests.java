package com.timka.contacteco.tests;

import static org.junit.Assert.*;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.util.List;

import org.junit.Test;

import com.amazonaws.services.simpledb.model.Item;
import com.timka.contacteco.DataManager;
import com.timka.contacteco.DomainName;
import com.timka.contacteco.Message;
import com.timka.contacteco.User;

public class AwsTests
{
	@Test
	public void testListUsers()
	{
		List<Item> items = DataManager.getInstance().getAllRecords(DomainName.User);
		printItems(items);
	}
	
	@Test
	public void testListMessages()
	{
		List<Item> items = DataManager.getInstance().getAllRecords(DomainName.Queue);
		printItems(items);
	}
	
	@Test
	public void testListDomains()
	{
		List<String> allTables = DataManager.getInstance().getTables();
		for (String table : allTables)
		{
			System.out.println(table);
		}
		assertTrue(allTables != null);
	}
	
	@Test
	public void testDeleteMessages()
	{
		DataManager.getInstance().dropTable(DomainName.Queue);
		DataManager.getInstance().createTable(DomainName.Queue);
		List<Item> items = DataManager.getInstance().getAllRecords(DomainName.Queue);
		assertTrue(items.size() == 0);
		
		testListMessages();
	}
	
	@Test
	public void testAddUser()
	{
		String login = "carol";
		String pass = encodePass("carolishere");
		
		String id = DataManager.getInstance().createUser(login, pass);
		assertTrue(id != null);
		
		User user = DataManager.getInstance().getUser(id);
		System.out.println("User : " + user);
		
		assertTrue(user != null);
		assertTrue(user.id == id);
		assertTrue(user.login == login);
		assertTrue(user.password == pass);
		
		testListUsers();
	}
	
	@Test
	public void testCreateFriendRequest()
	{
		String touserid = "d5bf9736-df91-46d8-85d6-885705c896cb";
		String fromuserid1 = "c018945b-5f21-4ed1-8efd-624420b249fb";
		String fromuserid2 = "e33e2248-5868-4248-8839-28df106c88bc";
		
		boolean created1 = DataManager.getInstance().createFriendRequest(fromuserid1, touserid);
		boolean created2 = DataManager.getInstance().createFriendRequest(fromuserid2, touserid);
		
		User user = DataManager.getInstance().getUser(touserid);
		assertTrue(user != null);
		
		if (created1 || created2)
			assertTrue(user.friendrequests != null);
		
		if (created1)
			assertTrue(contains(fromuserid1, user.friendrequests));
		
		if (created2)
			assertTrue(contains(fromuserid2, user.friendrequests));
		
		testListUsers();
	}
	
	@Test
	public void testCreateMessageToFriend()
	{
		String touserid = "d5bf9736-df91-46d8-85d6-885705c896cb";
		String fromuserid = "c018945b-5f21-4ed1-8efd-624420b249fb";
		String[] data = {
				"Test message to Bob from Alice # 1",
				"Test message to Bob from Alice # 2",
				"Test message to Bob from Alice # 3",
		};

		for (String d : data)
		{
			String id1 = DataManager.getInstance().createMessage(fromuserid, touserid, d);
			assertTrue(id1 != null);
			
			String id2 = DataManager.getInstance().createMessage(fromuserid, touserid, d);
			assertTrue(id2 == null);
		}
		
		testListMessages();
	}
	
	@Test
	public void testAcceptMessages()
	{
		String touserid = "d5bf9736-df91-46d8-85d6-885705c896cb";
		Message[] messages = DataManager.getInstance().getMessages(touserid);
		
		if (messages != null)
		{
			String[] messageids = new String[messages.length];
			for (int i = 0; i < messages.length; i++)
			{
				messageids[i] = messages[i].id;
			}
			DataManager.getInstance().acceptMessages(messageids);
			
			messages = DataManager.getInstance().getMessages(touserid);
			assertTrue(messages == null);
		}
		
		testListMessages();
	}
	
	@Test
	public void testCreateMessageToStranger()
	{
		String touserid = "d5bf9736-df91-46d8-85d6-885705c896cb";
		String fromuserid = "e33e2248-5868-4248-8839-28df106c88bc";
		String data = "Test message to Bob from Carol"; 

		String id = DataManager.getInstance().createMessage(fromuserid, touserid, data);
		assertTrue(id == null);
		
		testListMessages();
	}
	
	@Test
	public void testCreateFriendRequestWhenFriendExists()
	{
		String touserid = "d5bf9736-df91-46d8-85d6-885705c896cb";
		String fromuserid = "c018945b-5f21-4ed1-8efd-624420b249fb";
		
		boolean created = DataManager.getInstance().createFriendRequest(fromuserid, touserid);
		assertFalse(created);
		
		testListUsers();
	}
	
	@Test
	public void testUnfriend()
	{
		String touserid = "d5bf9736-df91-46d8-85d6-885705c896cb";
		String fromuserid = "e33e2248-5868-4248-8839-28df106c88bc";
		
		DataManager.getInstance().unfriend(fromuserid, touserid);
		DataManager.getInstance().unfriend(touserid, fromuserid);
		
		User touser = DataManager.getInstance().getUser(touserid);
		User fromuser = DataManager.getInstance().getUser(fromuserid);
		
		assertFalse(contains(fromuserid, touser.friends));
		assertFalse(contains(touserid, fromuser.friends));
		
		testListUsers();
	}
	
	@Test
	public void testAcceptFriendRequest()
	{
		String touserid = "d5bf9736-df91-46d8-85d6-885705c896cb";
		String fromuserid = "e33e2248-5868-4248-8839-28df106c88bc";
		
		DataManager.getInstance().acceptFriendRequests(new String[] { fromuserid }, touserid);
		
		User touser = DataManager.getInstance().getUser(touserid);
		User fromuser = DataManager.getInstance().getUser(fromuserid);
		
		assertTrue(touser != null);
		assertTrue(fromuser != null);
		
		assertFalse(contains(fromuserid, touser.friendrequests));
		assertTrue(contains(fromuserid, touser.friends));
		assertTrue(contains(touserid, fromuser.friends));
		
		testListUsers();
	}
	
	@Test
	public void testDeclineFriendRequests()
	{
		String touserid = "d5bf9736-df91-46d8-85d6-885705c896cb";
		String fromuserid = "e33e2248-5868-4248-8839-28df106c88bc";
		
		DataManager.getInstance().declineFriendRequests(new String[] { fromuserid }, touserid);
		
		User touser = DataManager.getInstance().getUser(touserid);
		User fromuser = DataManager.getInstance().getUser(fromuserid);
		
		assertTrue(touser != null);
		assertTrue(fromuser != null);
		
		assertFalse(contains(fromuserid, touser.friendrequests));
		assertFalse(contains(fromuserid, touser.friends));
		assertFalse(contains(touserid, fromuser.friends));
		
		testListUsers();
	}
	
	@Test
	public void testUpdatePass()
	{
		String id = "d5bf9736-df91-46d8-85d6-885705c896cb";
		String oldpass = "CY9rzUYh03PK3k6DJie09g==";
		String newpass = encodePass("bobishere");
		
		boolean result = DataManager.getInstance().updatePassword(id, oldpass, newpass);
		assertTrue(result);
		
		testListUsers();
	}
	
	@Test
	public void testEncodePass()
	{
		String pass = "1234567";
		System.out.println(encodePass(pass));
	}
	
	@Test
	public void testDeleteUser()
	{
		String userid = "5c7b68d5-3f49-4097-b71a-6fa604680510";
		DataManager.getInstance().deleteUser(userid);
		User user = DataManager.getInstance().getUser(userid);
		System.out.println("User : " + user);
		assertTrue(user == null);
		
		testListUsers();
	}
	
	private boolean contains(String needle, String[] haystack)
	{
		boolean exists = false;
		
		if (haystack != null)
		{
			for (String s : haystack)
			{
				if (s.equals(needle))
				{
					exists = true;
					break;
				}
			}
		}
		return exists;
	}
	
	private String encodePass(String pass)
	{
		try
		{
			byte[] bytes = pass.getBytes("UTF-8");
			MessageDigest md = MessageDigest.getInstance("MD5");
			md.update(bytes, 0, bytes.length);
			String md5 = new BigInteger(1, md.digest()).toString(16);
	        return md5;
		}
		catch (Exception x)
		{
			return null;
		}
	}
	
	private void printItems(List<Item> items)
	{
		System.out.println("Records : " + items.size());
		for (Item item : items)
		{
			System.out.println(item);
		}
	}
}
