package com.timka.contacteco;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.simpledb.*;
import com.amazonaws.services.simpledb.model.*;

public class DataManager
{
	
	private final String _ak = "AKIAJ7UJRIBVUUY7WQVQ";
	private final String _sak = "sMTvmA/kDNn3uXmPQjNUiM15CpsAqkTv6xGAmiqi";
    private AmazonSimpleDB _sdb = null;
    
    private boolean _consistentUsers = true;
    private boolean _consistentChecks = true;
    private boolean _consistentMessages = false;
    
    private DataManager()
    {
    	_sdb = new AmazonSimpleDBClient(new BasicAWSCredentials(_ak, _sak));
    }
    
    public static DataManager getInstance()
    {
    	return DataManagerInstance.getInstance();
    }

    public void createTable(String tableName)
    {
		_sdb.createDomain(new CreateDomainRequest().withDomainName(tableName));
    }
    
    public void dropTable(String tableName)
    {
		_sdb.deleteDomain(new DeleteDomainRequest().withDomainName(tableName));
    }
    
    public List<String> getTables()
    {
    	List<String> tables = new ArrayList<String>();
    	ListDomainsResult response = _sdb.listDomains();
    	if (response != null)
    	{
    		tables.addAll(response.getDomainNames());
    	}
    	return tables;
    }
    
	public boolean tableExists(String tableName)
	{
		DomainMetadataResult response = _sdb.domainMetadata(new DomainMetadataRequest().withDomainName(tableName));
		if (response.getItemCount() > 0)
		{
			return true;
		}
		return false;
	}

	private static final class DataManagerInstance
	{
		private static final DataManager _instance = new DataManager();

		public static DataManager getInstance()
		{
			return _instance;
		}
	}

	public String userExists(String login)
	{
		String query = MessageFormat.format("select itemName() from {0} where login = ''{1}''", DomainName.User, login);
		SelectRequest request = new SelectRequest().withSelectExpression(query).withConsistentRead(_consistentUsers);
		SelectResult response = _sdb.select(request);

		if (response != null && response.getItems() != null && !response.getItems().isEmpty())
		{
			return response.getItems().get(0).getName();
		}

		return null;
	}
	
	public void unfriend(String selfid, String friendid)
	{
		deleteAttribute(DomainName.User, selfid, "friend", friendid);
	}
	
	public boolean updatePassword(String id, String oldpass, String newpass)
	{
		GetAttributesRequest grequest = new GetAttributesRequest(DomainName.User, id).withAttributeNames("password").withConsistentRead(_consistentUsers);
		GetAttributesResult response = _sdb.getAttributes(grequest);
		if (response == null || response.getAttributes() == null || response.getAttributes().isEmpty() || !response.getAttributes().get(0).getValue().equals(oldpass))
		{
			return false;
		}
		
		PutAttributesRequest prequest = new PutAttributesRequest()
			.withDomainName(DomainName.User)
			.withItemName(id)
			.withAttributes(new ReplaceableAttribute().withName("password").withValue(newpass).withReplace(true));
		
		_sdb.putAttributes(prequest);
		
		return true;
	}
	
	public boolean createFriendRequest(String fromuserid, String touserid)
	{
		if (areFriends(fromuserid, touserid))
		{
			return false;
		}
		
		createAttributeOnItem(DomainName.User, touserid, "request", fromuserid);
		return true;
	}
	
	public void acceptFriendRequests(String[] fromuserids, String touserid)
	{
		createAttributesOnItem(DomainName.User, touserid, "friend", fromuserids);
		createAttributeOnItems(DomainName.User, fromuserids, "friend", touserid);
		deleteAttributes(DomainName.User, touserid, "request", fromuserids);
	}
	
	public void declineFriendRequests(String[] fromuserids, String touserid)
	{
		deleteAttributes(DomainName.User, touserid, "request", fromuserids);
	}
	
	public User getUser(String id)
	{
		User user = null;
		GetAttributesRequest request = new GetAttributesRequest(DomainName.User, id).withConsistentRead(_consistentUsers);
		GetAttributesResult response = _sdb.getAttributes(request);
		
		if (response != null)
		{
			List<Attribute> attributes = response.getAttributes();
			if (attributes != null && attributes.size() > 0)
			{
				user = new User();
				user.id = id;
				List<String> requests = new ArrayList<String>();
				List<String> friends = new ArrayList<String>();
				for (Attribute attribute : attributes)
				{
					if (attribute.getName().equals("login"))
						user.login = attribute.getValue();
					if (attribute.getName().equals("password"))
						user.password = attribute.getValue();
					if (attribute.getName().equals("created"))
						user.created = attribute.getValue();
					if (attribute.getName().equals("request"))
						requests.add(attribute.getValue());
					if (attribute.getName().equals("friend"))
						friends.add(attribute.getValue());
				}
				if (!friends.isEmpty())
				{
					user.friends = friends.toArray(new String[0]);
				}
				if (!requests.isEmpty())
				{
					user.friendrequests = requests.toArray(new String[0]);
				}
			}
		}
		
		return user;
	}
	
	public String createMessage(String fromuserid, String touserid, String data)
	{
		if (!areFriends(fromuserid, touserid))
		{
			return null;
		}
		if (messageExists(fromuserid, touserid, data))
		{
			return null;
		}
		
		String unixtime = String.valueOf(getUnixtime());
		String uid = UUID.randomUUID().toString();

		PutAttributesRequest request = new PutAttributesRequest()
			.withDomainName(DomainName.Queue)
			.withItemName(uid)
			.withAttributes(new ReplaceableAttribute().withName("type").withValue(MessageType.VcardData),
					new ReplaceableAttribute().withName("fromuserid").withValue(fromuserid),
					new ReplaceableAttribute().withName("touserid").withValue(touserid),
					new ReplaceableAttribute().withName("created").withValue(unixtime),
					new ReplaceableAttribute().withName("data").withValue(data));

		_sdb.putAttributes(request);

		return uid;
	}
	
	public Message[] getMessages(String touserid)
	{
		Message[] messages = null;
		
		String query = MessageFormat.format("select * from {0} where touserid = ''{1}'' and type = ''{2}'' and created is not null order by created", DomainName.Queue, touserid, MessageType.VcardData);
		SelectRequest request = new SelectRequest().withSelectExpression(query).withConsistentRead(_consistentMessages);
		SelectResult response = _sdb.select(request);
		
		if (response != null && response.getItems() != null && !response.getItems().isEmpty())
		{
			List<Item> items = response.getItems();
			messages = new Message[items.size()];
			for (int i = 0; i < messages.length; i++)
			{
				messages[i] = new Message();
				messages[i].id = items.get(i).getName();
				for (Attribute attribute : items.get(i).getAttributes())
				{
					if (attribute.getName().equals("fromuserid"))
						messages[i].fromuserid = attribute.getValue();
					if (attribute.getName().equals("created"))
						messages[i].created = attribute.getValue();
					if (attribute.getName().equals("data"))
						messages[i].data = attribute.getValue();
				}
			}
		}
		
		return messages;
	}
	
	public String createUser(String login, String pass)
	{
		if (userExists(login) != null)
		{
			return null;
		}
		
		String id = UUID.randomUUID().toString();
		String unixtime = String.valueOf(getUnixtime());
		
		PutAttributesRequest request = new PutAttributesRequest()
			.withDomainName(DomainName.User)
			.withItemName(id)
			.withAttributes(
				new ReplaceableAttribute().withName("type").withValue("0"),
				new ReplaceableAttribute().withName("login").withValue(login),
				new ReplaceableAttribute().withName("password").withValue(pass),
				new ReplaceableAttribute().withName("created").withValue(unixtime));
	
		_sdb.putAttributes(request);
		
		return id;
	}
	
	public void acceptMessages(String[] messageids)
	{
		deleteItems(DomainName.Queue, messageids);
	}
	
	public boolean deleteUser(String userid)
	{
		return deleteItems(DomainName.User, new String[] { userid });
	}
	
	public Item getRecord(String tableName, String itemName)
	{
		String query = MessageFormat.format("select * from {0} where itemName() = ''{1}''", tableName, itemName);
		SelectRequest request = new SelectRequest().withSelectExpression(query).withConsistentRead(_consistentUsers);
		SelectResult response = _sdb.select(request);

		if (response != null)
		{
			return response.getItems().get(0);
		}

		return null;
	}
	
	public List<Item> getAllRecords(String tableName)
	{
		String query = MessageFormat.format("select * from {0}", tableName);
		SelectRequest request = new SelectRequest().withSelectExpression(query).withConsistentRead(_consistentUsers);
		SelectResult response = _sdb.select(request);

		if (response != null)
		{
			return response.getItems();
		}

		return null;
	}
	
	private boolean messageExists(String fromuserid, String touserid, String data)
	{
		String query = MessageFormat.format("select itemName() from {0} where fromuserid = ''{1}'' and touserid = ''{2}'' and data = ''{3}''", DomainName.Queue, fromuserid, touserid, data);
		SelectRequest request = new SelectRequest().withSelectExpression(query).withConsistentRead(_consistentChecks);
		SelectResult response = _sdb.select(request);
		
		if (response != null && response.getItems() != null && !response.getItems().isEmpty())
		{
			return true;
		}
		
		return false;
	}

	private boolean areFriends(String sender, String recipient)
	{
		GetAttributesRequest request = new GetAttributesRequest(DomainName.User, recipient).withAttributeNames("friend").withConsistentRead(_consistentChecks);
		GetAttributesResult response = _sdb.getAttributes(request);
		
		boolean exists = false;
		if (response != null && response.getAttributes() != null)
		{
			for (Attribute attr : response.getAttributes())
			{
				if (attr.getValue().equals(sender))
				{
					exists = true;
					break;
				}
			}
		}
		return exists;
	}

	private void deleteAttribute(String domainName, String itemName, String attrName, String attrValue)
	{
		DeleteAttributesRequest drequest = new DeleteAttributesRequest()
			.withDomainName(domainName)
			.withItemName(itemName)
			.withAttributes(new Attribute(attrName, attrValue));
	
		_sdb.deleteAttributes(drequest);
	}
	
	private void deleteAttributes(String domainName, String itemName, String attrName, String[] attrValues)
	{
		BatchDeleteAttributesRequest request = new BatchDeleteAttributesRequest()
			.withDomainName(domainName);
		
		for (String attrValue : attrValues)
		{
			request = request.withItems(new DeletableItem().withName(itemName).withAttributes(new Attribute(attrName, attrValue)));
		}
	
		_sdb.batchDeleteAttributes(request);
	}
	
	private void createAttributeOnItem(String domainName, String itemName, String attrName, String attrValue)
	{
		PutAttributesRequest prequest = new PutAttributesRequest()
			.withDomainName(domainName)
			.withItemName(itemName)
			.withAttributes(new ReplaceableAttribute().withName(attrName).withValue(attrValue));

		_sdb.putAttributes(prequest);
	}
	
	private void createAttributesOnItem(String domainName, String itemName, String attrName, String[] attrValues)
	{
		PutAttributesRequest request = new PutAttributesRequest()
			.withDomainName(domainName)
			.withItemName(itemName);
		
		for (String attrValue : attrValues)
		{
			request = request.withAttributes(new ReplaceableAttribute(attrName, attrValue, false));
		}

		_sdb.putAttributes(request);
	}
	
	private void createAttributeOnItems(String domainName, String[] itemNames, String attrName, String attrValue)
	{
		if (itemNames != null && itemNames.length == 0)
		{
			createAttributeOnItem(domainName, itemNames[0], attrName, attrValue);
			return;
		}
		
		BatchPutAttributesRequest request = new BatchPutAttributesRequest()
			.withDomainName(domainName);
		
		ReplaceableAttribute attr = new ReplaceableAttribute().withName(attrName).withValue(attrValue);
		
		for (String itemName : itemNames)
		{
			request = request.withItems(new ReplaceableItem().withName(itemName).withAttributes(attr));
		}

		_sdb.batchPutAttributes(request);
	}
	
	private boolean deleteItems(String domainName, String[] itemNames)
	{
		try
		{
			BatchDeleteAttributesRequest request = new BatchDeleteAttributesRequest()
				.withDomainName(domainName);
			
			for (String itemName : itemNames)
			{
				request = request.withItems(new DeletableItem().withName(itemName));
			}
			
			_sdb.batchDeleteAttributes(request);
			
			return true;
		}
		catch (Exception x)
		{
			x.printStackTrace();
			return false;
		}
	}
	
	private long getUnixtime()
	{
		return System.currentTimeMillis() / 1000L;
	}
}
