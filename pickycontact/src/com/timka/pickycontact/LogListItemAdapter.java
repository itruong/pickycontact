package com.timka.pickycontact;

import java.util.ArrayList;

import com.timka.pickycontact.R;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class LogListItemAdapter extends ArrayAdapter<LogListItem>{

    Context context; 
    int layoutResourceId;    
    ArrayList<LogListItem> data = null;
    
    public LogListItemAdapter(Context context, int layoutResourceId) {
        super(context, layoutResourceId);
        this.layoutResourceId = layoutResourceId;
        this.context = context;
        this.data = new ArrayList<LogListItem>();
    }
    
    @Override
    public void add(LogListItem object)
    {
        data.add(object);
        this.notifyDataSetChanged();
    }

    @Override
    public int getCount()
    {
        return data.size();
    }

    @Override
    public LogListItem getItem(int position)
    {
        return data.get(position);
    }
    
    @Override
    public void clear()
    {
        data.clear();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        View row = convertView;
        LogListItemHolder holder = null;
        
        if (row == null)
        {
            LayoutInflater inflater = ((Activity)context).getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);
            
            holder = new LogListItemHolder();
            holder.txtLine1 = (TextView) row.findViewById(R.id.txtLine1);
            holder.txtLine2 = (TextView) row.findViewById(R.id.txtLine2);
            
            row.setTag(holder);
        }
        else
        {
            holder = (LogListItemHolder) row.getTag();
        }
        
        LogListItem item = getItem(position);
        holder.txtLine1.setText(item.Line1);
        holder.txtLine2.setText(item.Line2);
        
        return row;
    }
    
    static class LogListItemHolder
    {
        TextView txtLine1;
        TextView txtLine2;
    }
}