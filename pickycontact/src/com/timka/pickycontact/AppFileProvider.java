package com.timka.pickycontact;

import java.io.File;
import java.io.FileNotFoundException;

import com.timka.pickycontact.util.Logger;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.net.Uri;
import android.os.ParcelFileDescriptor;

public class AppFileProvider extends ContentProvider
{
    public static final String AUTHORITY = "com.timka.pickycontact.provider";
    private UriMatcher uriMatcher;

    @Override
    public boolean onCreate()
    {
        uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        uriMatcher.addURI(AUTHORITY, "*", 1);
        return false;
    }
    
    @Override
    public ParcelFileDescriptor openFile(Uri uri, String mode) throws FileNotFoundException
    {
        Logger.debug("Provider called with uri: '" + uri + "'.");
 
        switch (uriMatcher.match(uri))
        {
            case 1:
                File outDir = getContext().getDir(App.PATH_SENT, Context.MODE_WORLD_READABLE);
                String fileLocation = outDir.getAbsolutePath() + File.separator + uri.getLastPathSegment();
                ParcelFileDescriptor pfd = ParcelFileDescriptor.open(
                    new File(fileLocation),
                    ParcelFileDescriptor.MODE_READ_ONLY);
                return pfd;
        default:
            Logger.debug("Unsupported uri: '" + uri + "'.");
            throw new FileNotFoundException("Unsupported uri: " + uri.toString());
        }
    }
    
    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs)
    {
        return 0;
    }

    @Override
    public Uri insert(Uri uri, ContentValues values)
    {
        return null;
    }

    @Override
    public String getType(Uri uri)
    {
        return null;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder)
    {
        return null;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs)
    {
        return 0;
    }
    
}
