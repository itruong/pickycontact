package com.timka.pickycontact;

import com.timka.pickycontact.util.StringUtils;

import android.app.Application;

public class App extends Application
{
    public static final int RESULT_CONTACT_PICKER = 1001;
    public static final int RESULT_CONTACT_FIELDS = 1002;
    
    public static final String EXTRA_CONTACT_URI = "com.timka.pickycontact.CONTACT_URI"; 
    
    public static final String PROP_CONTACT_NAME = "CONTACT_NAME";
    
    public static final String PATH_SENT = "sent";
    
    public App()
    {
        super();
        StringUtils.init(this);
    }
}
