package com.timka.pickycontact;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Properties;
import java.util.Set;

import com.timka.pickycontact.R;
import com.timka.pickycontact.util.Logger;

import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.provider.ContactsContract.Contacts;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.AssetFileDescriptor;
import android.content.res.Resources;
import android.database.Cursor;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.SimpleAdapter;

public class ContactActivity extends Activity {
    
    private static SimpleAdapter adapter = null;
    
    private static VCardResult vcard = null;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact);
        
        vcard = new VCardResult(this);
        
        readContactIfSelected();
        retrieveContactFields();
        
        ListView lstFields = (ListView) findViewById(R.id.lstFields);
        
        adapter = new SimpleAdapter(
                this,
                vcard.getFields(),
                R.layout.checked_item,
                new String[] {"line1","line2" },
                new int[] { R.id.cbName, R.id.txtValue } );
        
        lstFields.setAdapter(adapter);
        lstFields.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
        lstFields.setClickable(true);
        lstFields.setOnItemClickListener(new OnItemClickListener()
        {
            public void onItemClick(AdapterView<?> parent, View view, int position, long id)
            {
                CheckBox cb = (CheckBox) view.findViewById(R.id.cbName);
                if (cb.isChecked() && vcard.isRequired(position))
                {
                    return;
                }
                vcard.setChecked(position, !cb.isChecked());
                cb.setChecked(!cb.isChecked());
            }
        });
    }
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        getMenuInflater().inflate(R.menu.activity_contact, menu);
        return true;
    }
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
    	switch(item.getItemId())
    	{
    		case R.id.menu_help_contact:
    			showDialog(item.getItemId());
    			return true;
    	}
    	return super.onOptionsItemSelected(item);
    }
    
	@Override
	protected Dialog onCreateDialog(int id)
	{
		switch (id)
		{
			case R.id.menu_help_contact:
			{
				return new AlertDialog.Builder(this)
						.setMessage(R.string.menu_help_contact)
						.setPositiveButton("OK",
								new DialogInterface.OnClickListener()
								{
									public void onClick(DialogInterface dialog, int result)
									{
									}
								}).create();
			}
		}
		return null;
	}
    
    public void onFieldsSelected(View sender)
    {
        try
        {
        	vcard.write();
            vcard.send();
            Intent intent = new Intent();
            setResult(RESULT_OK, intent);
            finish();
        }
        catch (Exception x)
        {
            x.printStackTrace();
        }
    }
    
    private void readContactIfSelected()
    {
        Intent intent = getIntent();
        if (intent.getAction() == Intent.ACTION_SEND)
        {
        	if (intent.getExtras().containsKey(Intent.EXTRA_STREAM))
        	{
        		Uri vcardUri = (Uri) intent.getExtras().getParcelable(Intent.EXTRA_STREAM);
            	if (vcardUri != null)
            	{
            		Uri contactUri = convertVcardUri(vcardUri);
            		if (contactUri != null)
            		{
            			Logger.debug("Selected contact: " + contactUri);
            			intent.putExtra(App.EXTRA_CONTACT_URI, contactUri.toString());
            		}
            	}
        	}
        }
    }
    
    private Uri convertVcardUri(Uri vcardUri)
    {
        Cursor cursor = managedQuery(
                ContactsContract.Contacts.CONTENT_URI, 
                null,
                ContactsContract.Contacts.LOOKUP_KEY + " = ?", 
                new String [] { vcardUri.getLastPathSegment() }, 
                null);
        
        if (cursor != null && cursor.moveToFirst())
        {
            String id = cursor.getString(cursor.getColumnIndex(Contacts._ID));
            Uri baseUri = ContactsContract.Contacts.CONTENT_LOOKUP_URI;
        	Uri contactUri = Uri.withAppendedPath(baseUri, vcardUri.getLastPathSegment());
        	contactUri = Uri.withAppendedPath(contactUri, id);
        	return contactUri;
        }
        return null;
    }
    
    private void retrieveContactFields()
    {
        Bundle extras = getIntent().getExtras();
        if (extras != null)
        {
            Uri contactUri = Uri.parse(extras.getString(App.EXTRA_CONTACT_URI));
            String contactId = contactUri.getLastPathSegment();
            
            Cursor cursor = managedQuery(
                    ContactsContract.Contacts.CONTENT_URI, 
                    null,
                    "_id = ?", 
                    new String [] { contactId }, 
                    null);
            
            if (cursor != null && cursor.moveToFirst())
            {
                String contactName = cursor.getString(cursor.getColumnIndex(Contacts.DISPLAY_NAME));
                String lookupKey = cursor.getString(cursor.getColumnIndex(Contacts.LOOKUP_KEY));
                Uri uri = Uri.withAppendedPath(ContactsContract.Contacts.CONTENT_VCARD_URI, lookupKey);
                vcard.read(uri, contactId, contactName);
            }
        }
    }
}
