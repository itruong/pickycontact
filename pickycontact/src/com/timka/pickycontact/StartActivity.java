package com.timka.pickycontact;

import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.text.MessageFormat;
import java.util.Date;
import java.util.Properties;

import com.timka.pickycontact.R;
import com.timka.pickycontact.util.Logger;

import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.provider.ContactsContract.Contacts;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.database.Cursor;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;

public class StartActivity extends Activity {
    
    private LogListItemAdapter adapter = null;
    ListView lstSent = null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        setContentView(R.layout.activity_start);
        
        adapter = new LogListItemAdapter(this, R.layout.listview_item);
        
        lstSent = (ListView) findViewById(R.id.lstSent);
        lstSent.setEmptyView(findViewById(R.id.txtSentEmpty));
        lstSent.setAdapter(adapter);
        readSentFiles();
    }
    
    public void openContacts(View sender)
    {
        Logger.debug("Opening contacts");
        Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
        startActivityForResult(intent, App.RESULT_CONTACT_PICKER);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_start, menu);
        return true;
    }
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
    	switch(item.getItemId())
    	{
    		case R.id.menu_help_start:
    			showDialog(item.getItemId());
    			return true;
    	}
    	return super.onOptionsItemSelected(item);
    }
    
	@Override
	protected Dialog onCreateDialog(int id)
	{
		switch (id)
		{
			case R.id.menu_help_start:
			{
				return new AlertDialog.Builder(this)
						.setMessage(R.string.menu_help_start)
						.setPositiveButton("OK",
								new DialogInterface.OnClickListener()
								{
									public void onClick(DialogInterface dialog, int result)
									{
									}
								}).create();
			}
		}
		return null;
	}

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK)
        {
            switch (requestCode)
            {
                case App.RESULT_CONTACT_PICKER:
                    selectFields(data);
                    break;
                    
                case App.RESULT_CONTACT_FIELDS:
                    readSentFiles();
                    break;
            }
        }
        else
        {
            Logger.debug(MessageFormat.format("Contact picker result code: {0}", resultCode));
        }
    }
    
    private void selectFields(Intent data)
    {
        Uri contactUri = data.getData();
        
        if (contactUri != null)
        {
            Logger.debug(MessageFormat.format("Contact URI: {0}", contactUri));
            Intent intent = new Intent(this, ContactActivity.class);
            intent.putExtra(App.EXTRA_CONTACT_URI, contactUri.toString());
            startActivityForResult(intent, App.RESULT_CONTACT_FIELDS);
        }
    }

    private void readSentFiles()
    {
        File dir = getDir(App.PATH_SENT, MODE_WORLD_READABLE);
        File[] files = dir.listFiles(new PropertyFileFilter());
        
        if (files != null)
        {
            Logger.debug(MessageFormat.format("Files sent: {0}", files.length));
            adapter.clear();
            
            Properties properties = new Properties();
            Resources r = getResources();

            for (File file : files)
            {
                try
                {
                    properties.load(new FileInputStream(file.getAbsolutePath()));
                    String contactName = properties.getProperty(App.PROP_CONTACT_NAME);
                    
                    String fileName = file.getName().split("\\.")[0];
                    String unixtime = fileName.split("\\-")[0];
                    Date datetime = getDate(Long.parseLong(unixtime));
                    
                    adapter.add(new LogListItem(
                            MessageFormat.format(r.getString(R.string.sent_list_line_1), contactName),
                            MessageFormat.format(r.getString(R.string.sent_list_line_2), datetime)));
                }
                catch (Exception x)
                {
                    x.printStackTrace();
                }
            }
        }
        else
        {
            Logger.debug("Files sent: 0");
        }
    }
    
    public static class PropertyFileFilter implements FileFilter
    {
        public boolean accept(File pathname)
        {
            if (pathname.getName().toLowerCase().endsWith(".properties"))
            {
                return true;
            }
            return false;
        }
    }
    
    private Date getDate(long unixtime)
    {
        return new Date(unixtime * 1000);
    }
}
