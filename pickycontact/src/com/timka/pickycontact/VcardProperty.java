package com.timka.pickycontact;

import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import com.timka.pickycontact.util.Logger;
import com.timka.pickycontact.util.StringUtils;

public class VcardProperty
{
	public String Key = null;
	public String Txt = null;
	public String FullKey = null;
	
	private String rawKey = null;
	private String rawTxt = null;
	
	private String[] keyParts = null;
	private String[] txtParts = null;
	private String[] headers = null;
	private ArrayList<String> keyParams = new ArrayList<String>(); 
	
	private String encoding = null;
	private Charset currentCharset = null;
	
	private static final Charset defaultCharset = Charset.forName("UTF-8");
	private static final Set<String> hiddenHeaders = new HashSet<String>();
	
	public VcardProperty(String key, String value)
	{
		hiddenHeaders.add("CHARSET");
		hiddenHeaders.add("ENCODING");
		hiddenHeaders.add("PREF");
		
		rawKey = key;
		rawTxt = value;
		parse();
	}
	
	private void parse()
	{
		keyParts = rawKey.split(";");
		txtParts = rawTxt.split(";");
		
		Key = format(keyParts[0]);
		
		parseHeaders();
		parseContent();
	}
	
	private void parseHeaders()
	{
		headers = new String[keyParts.length];
		keyParams = new ArrayList<String>();
		for (int i = 1; i < headers.length; i++)
		{
			headers[i] = parseHeaderValue(keyParts[i]);
		}
		if (keyParams.size() > 0)
		{
			FullKey = Key + " (" + StringUtils.join(keyParams, ", ") + ")"; 
		}
		else
		{
			FullKey = Key;
		}
	}
	
	private void parseContent()
	{
		StringBuilder sb = new StringBuilder();
		for (String s : txtParts)
		{
			if (s != null && s.length() > 0)
			{
				if (isQuotedPrintable())
				{
					if (s.endsWith("="))
					{
						s = s.substring(0, s.length() - 1);
					}
				}
				else if (sb.length() > 0)
				{
					sb.append(", ");
				}
				sb.append(s);
			}
		}
		Txt = decodeText(sb.toString());
	}
	
	private boolean isQuotedPrintable()
	{
		return encoding != null && encoding.compareToIgnoreCase("QUOTED-PRINTABLE") == 0;
	}
	
	private boolean isBase64()
	{
		return encoding != null && encoding.compareToIgnoreCase("BASE64") == 0;
	}
	
	private boolean isBinary()
	{
		return encoding != null && encoding.compareToIgnoreCase("B") == 0;
	}
	
	private String decodeText(String text)
	{
		if (encoding != null)
		{
			if (isBinary() || isBase64())
			{
				return "";
			}
			if (isQuotedPrintable())
			{
				return convertQuotedPrintable(text);
			}
			else
			{
				return convertFromEncoding(text);
			}
		}
		return text;
	}
	
	private String convertFromEncoding(String s)
	{
		try
		{
			Charset charset = Charset.forName(encoding);
			CharsetDecoder decoder = charset.newDecoder();
			byte[] bytes = s.getBytes(charset);
			ByteBuffer bbuf = ByteBuffer.wrap(bytes);
			CharBuffer cbuf = decoder.decode(bbuf);
		    return cbuf.toString();
		}
		catch (Exception x)
		{
			Logger.debug("Could not decode: " + encoding);
			x.printStackTrace();
			return "";
		}
	}
	
	private String convertQuotedPrintable(String s)
	{
		Charset set = (currentCharset == null) ? defaultCharset : currentCharset;
		byte[] decoded = QuotedPrintable.decodeQuotedPrintable(s.getBytes(set));
		return new String(decoded, defaultCharset);
	}
	
	private String parseHeaderValue(String header)
	{
		if (header.contains("="))
		{
			String[] parts = header.split("=", 2);
			String value = unquote(parts[1]);
			if (parts[0].compareTo("CHARSET") == 0)
			{
				parseCharset(value);
			}
			if (parts[0].compareToIgnoreCase("ENCODING") == 0)
			{
				encoding = value;
			}
			if (isVisibleHeader(parts[0]))
			{
				keyParams.add(value.toLowerCase());
			}
			return value;
		}
		else if (isVisibleHeader(header))
		{
			keyParams.add(header.toLowerCase());
		}
		return header;
	}

	private boolean isVisibleHeader(String s)
	{
		return (!hiddenHeaders.contains(s.toUpperCase()));
	}

	private void parseCharset(String charsetName)
	{
		try
		{
			currentCharset = Charset.forName(charsetName);
		}
		catch (Exception x)
		{
			currentCharset = null;
			Logger.debug("Could not create charset: " + charsetName);
		}
	}

	private String unquote(String s)
	{
		if (s.startsWith("\"") && s.endsWith("\""))
		{
			return s.substring(1, s.length() - 1);
		}
		return s;
	}

	private String format(String key)
	{
		String decoded = StringUtils.get(key);
		if (decoded == null)
			decoded = key;
		return decoded;
	}
}
