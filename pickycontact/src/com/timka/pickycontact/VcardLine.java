package com.timka.pickycontact;

import java.util.ArrayList;

public class VcardLine
{
	public ArrayList<String> Original = new ArrayList<String>();
	public String Unfolded = null;
	
	public VcardLine(ArrayList<String> buffer)
	{
		this.Original = new ArrayList<String>();
		this.Original.addAll(buffer);
		
		if (buffer.size() == 1)
		{
			Unfolded = buffer.get(0);
		}
		else
		{
			Unfolded = unfold(buffer);
		}
	}
	
	public String unfold(ArrayList<String> buffer)
	{
		boolean isQuotedPrintable = checkQuotedPrintable();
		StringBuffer tmp = new StringBuffer();
		for (String line : buffer)
		{
			if (line.startsWith("\t") || line.startsWith(" "))
			{
				line = line.substring(1);
			}
			if (isQuotedPrintable && line.endsWith("="))
			{
				line = line.substring(0, line.length() - 1);
			}
			tmp.append(line);
		}
		return tmp.toString();
	}
	
	private boolean checkQuotedPrintable()
	{
		String key = Original.get(0).split("\\:")[0];
		String[] keyParts = key.split(";");
		for (String property : keyParts)
		{
			if (property.contains("="))
			{
				property = property.split("=", 2)[1];
			}
			if (property.toUpperCase().contains("QUOTED-PRINTABLE"))
			{
				return true;
			}
		}
		return false;
	}
}
