package com.timka.pickycontact;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.MessageFormat;

import com.timka.pickycontact.util.Logger;

import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.net.Uri;

public class VcardDebug
{
	private static final boolean DEBUG = false;
	
	private VcardDebug()
	{
		
	}
	
	public static void write(Context context, Uri uri, String contactName)
	{
		if (VcardDebug.DEBUG == false)
			return;
		
		try
		{
			File outDir = context.getDir(App.PATH_SENT, Context.MODE_WORLD_READABLE);
			File out = new File(outDir.getAbsolutePath() + File.separator + "debug-" + contactName + ".vcf");
			if (out.exists())
			{
				out.delete();
			}
			
	        AssetFileDescriptor fd = context.getContentResolver().openAssetFileDescriptor(uri, "r");
	        FileInputStream fis = fd.createInputStream();
	        FileOutputStream fos = new FileOutputStream(out);
	        
	        Logger.debug(MessageFormat.format("Debug: writing {0} bytes to {1}", fd.getLength(), out.getAbsolutePath()));
	        
	        byte [] buffer = new byte[512];
	        int read = 0;
	        try
	        {
	        	while ((read = fis.read(buffer)) != -1)
	            {
	        		fos.write(buffer, 0, read);
	            }        
	        }
	        finally
	        {
	        	tryCloseStream(fis);
	        	tryCloseStream(fos);
	        	fd.close();
	        }
	        Logger.debug(MessageFormat.format("Debug: wrote {0} bytes to {1}", fd.getLength(), out.getAbsolutePath()));
		}
    	catch (Exception x)
    	{
    		x.printStackTrace();
    	}
	}
	
	private static void tryCloseStream(FileInputStream fs)
	{
        try
        {
        	if (fs != null) fs.close();
        }
        catch ( IOException x)
        {
        }
	}
	
	private static void tryCloseStream(FileOutputStream fs)
	{
        try
        {
        	if (fs != null) fs.close();
        }
        catch ( IOException x)
        {
        }
	}
}
