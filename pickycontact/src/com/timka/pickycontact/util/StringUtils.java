package com.timka.pickycontact.util;

import java.util.List;

import android.content.Context;
import android.content.res.Resources;

public class StringUtils
{
    private static Context context = null;
    
    private StringUtils()
    {
    }
    
    public static void init(Context context)
    {
        StringUtils.context = context;
        Logger.debug("StringUtils: initialized");
    }
    
    public static String get(String resourceKey)
    {
        return get("string", resourceKey);
    }
    
    public static String get(String resourceType, String resourceKey)
    {
        Resources r = StringUtils.context.getResources();
        int id = r.getIdentifier(resourceKey, resourceType, "com.timka.pickycontact");
        if (id == 0)
        {
            return null;
        }
        else
        {
            String message = (String) r.getText(id);
            return message;
        }
    }
    
    public static String join(List<String> s, String delimiter)
    {
        if (s == null || s.isEmpty())
            return "";
        
        StringBuilder builder = new StringBuilder(s.get(0));
        for (int i = 1; i < s.size(); i++)
        {
            builder.append(delimiter).append(s.get(i));
        }
        return builder.toString();
    }
}
