package com.timka.pickycontact.util;

import android.util.Log;

public class Logger
{
    public static final String LOGTAG = "pickycontact";
    
    private Logger()
    {
    }
    
    public static void debug(String message)
    {
        Log.d(LOGTAG, message);
    }
}
