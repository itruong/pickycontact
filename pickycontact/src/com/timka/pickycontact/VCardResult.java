package com.timka.pickycontact;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Properties;
import java.util.Set;

import android.content.Context;
import android.content.Intent;
import android.content.res.AssetFileDescriptor;
import android.content.res.Resources;
import android.net.Uri;

import com.timka.pickycontact.util.Logger;

public class VCardResult
{
    private static ArrayList<HashMap<String,String>> fields = new ArrayList<HashMap<String,String>>();
    
    private static ArrayList<String> header = new ArrayList<String>();
    private static ArrayList<VcardLine> body = new ArrayList<VcardLine>();
    private static ArrayList<String> footer = new ArrayList<String>();
    private static ArrayList<Integer> required = new ArrayList<Integer>();
    
    private static final Set<String> headers = new HashSet<String>();
    private static final Set<String> footers = new HashSet<String>();
    private static final Set<String> reqs = new HashSet<String>();
    private static boolean[] checked = null;
    
    private static Context context = null;
    
    private static String contactName = null;
    private static String contactId = null;
    private static String contactFile = null;
    private static String propertyFile = null;
    
    public VCardResult(Context ctx)
    {
    	context = ctx;
        headers.clear();
        footers.clear();
        reqs.clear();
        
        headers.add("BEGIN");
        headers.add("VERSION");
        footers.add("END");
        reqs.add("N");
        reqs.add("FN");
    }
    
    public ArrayList<HashMap<String,String>> getFields()
    {
    	return fields;
    }
    
    public String getContactName()
    {
    	return contactName;
    }
    
    public String getContactFile()
    {
    	return contactFile;
    }
    
    public boolean getChecked(int position)
    {
    	return checked[position];
    }
    
    public void setChecked(int position, boolean value)
    {
    	checked[position] = value;
    }
    
    public boolean isRequired(int position)
    {
    	return required.contains(position);
    }
    
    public int getLineCount()
    {
    	return body.size();
    }
    
    private boolean isRequired(String line)
    {
        return reqs.contains(line);
    }
    
    private boolean isFooter(String line)
    {
        return footers.contains(line);
    }
    
    private boolean isHeader(String line)
    {
        return headers.contains(line);
    }
    
    private long getUnixtime()
    {
        return System.currentTimeMillis() / 1000L;
    }
    
    public void read(Uri uri, String id, String name)
    {
        try
        {
        	contactId = id;
        	contactName = name;
        	
            VcardDebug.write(context, uri, contactName);
            
            ArrayList<VcardLine> lines = readVcardLines(uri);
            
            header.clear();
            body.clear();
            footer.clear();
            fields.clear();
            for (VcardLine line : lines)
            {
                String[] parts = (line.Unfolded.contains(":")) ? line.Unfolded.split("\\:", 2) : new String [] { line.Unfolded };
                
                if (isHeader(parts[0]))
                {
                    header.add(line.Unfolded);
                }
                else if (isFooter(parts[0]))
                {
                    footer.add(line.Unfolded);
                }
                else
                {
                    parseBodyLine(line, parts);
                }
            }
            checked = new boolean[body.size()];
            for (int i = 0; i < checked.length; i++)
            {
                checked[i] = true;
            }
            
            Logger.debug(MessageFormat.format("Parsed {0} lines", body.size()));
        }
        catch (Exception x)
        {
            x.printStackTrace();
        }
    }
    
    public void write() throws UnsupportedEncodingException, FileNotFoundException, IOException
    {
        long unixtime = getUnixtime();
        contactFile = getFileName(unixtime, contactId);
        propertyFile = getPropertyFileName(unixtime, contactId);
        
        Logger.debug("Writing to: " + contactFile);
        
        toFile(contactFile);
        toPropertyFile(propertyFile);
            
        Logger.debug(MessageFormat.format("Wrote {0} lines to {1}", getLineCount(), contactFile));
    }
    
    public void send()
    {
        Resources r = context.getResources();
        
        String subject = r.getString(R.string.email_subject);
        String message = r.getString(R.string.email_message_1) + '\n'
                + r.getString(R.string.email_message_2) + '\n'
                + r.getString(R.string.email_message_3);
        
        Uri uri = Uri.parse("content://" + AppFileProvider.AUTHORITY + "/" + contactFile);
        
        try
        {
            Intent emailIntent = new Intent(Intent.ACTION_SEND);
            emailIntent.setType("plain/text");
            emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, MessageFormat.format(subject, contactName));
            emailIntent.putExtra(android.content.Intent.EXTRA_TEXT, message);
            emailIntent.putExtra(android.content.Intent.EXTRA_STREAM, uri);
            
            context.startActivity(Intent.createChooser(emailIntent, r.getString(R.string.email_picker_title)));
        }
        catch (Exception x)
        {
            x.printStackTrace();
        }
    }
    
    private void parseBodyLine(VcardLine line, String[] parts)
    {
        if (isRequired(parts[0]))
        {
            required.add(body.size());
        }
        
        body.add(line);

        String text = null;
        String key = null;
        
        if (parts.length > 1)
        {
            VcardProperty property = new VcardProperty(parts[0], parts[1]);
            text = property.Txt;
            key = property.FullKey;
        }
        else
        {
            Logger.debug("Could not parse:");
            Logger.debug(parts[0]);
        }
        
        HashMap<String,String> map = new HashMap<String,String>();
        map.put("line1", key);
        map.put("line2", text);
        fields.add(map);
    }
    
    private String getFileName(long unixtime, String id)
    {
        return unixtime + "-" + contactId + ".vcf";
    }
    
    private String getPropertyFileName(long unixtime, String id)
    {
        return unixtime + "-" + contactId + ".properties";
    }

    private void toPropertyFile(String propertyFile)
    {
        try
        {
            File outDir = context.getDir(App.PATH_SENT, Context.MODE_WORLD_READABLE);
            File fout = new File(outDir.getAbsolutePath() + File.separator + propertyFile);
            fout.createNewFile();
            
            Properties properties = new Properties();
            properties.put(App.PROP_CONTACT_NAME, contactName);
            properties.store(new FileOutputStream(fout), null);
        }
        catch (Exception x)
        {
            x.printStackTrace();
        }
    }
    
    private ArrayList<VcardLine> readVcardLines(Uri uri)
    {
        ArrayList<VcardLine> lines = new ArrayList<VcardLine>();
        try
        {
            AssetFileDescriptor fd = context.getContentResolver().openAssetFileDescriptor(uri, "r");
            BufferedReader br = new BufferedReader(new InputStreamReader(fd.createInputStream(), Charset.forName("UTF-8")));
            
            String current = null;
            ArrayList<String> buffer = new ArrayList<String>();
            
            while ((current = br.readLine()) != null)
            {
                if (buffer.size() > 0)
                {
                    if (current.startsWith("\t") || current.startsWith(" "))
                    {
                        // we have a folded line
                        buffer.add(current);
                    }
                    else if (!current.contains(":"))
                    {
                        // yet again folded line
                        buffer.add(current);
                    }
                    else
                    {
                        lines.add(new VcardLine(buffer));
                        
                        buffer.clear();
                        buffer.add(current);
                    }
                }
                else
                {
                    buffer.add(current);
                }
            }
            if (buffer.size() > 0)
            {
                lines.add(new VcardLine(buffer));
            }
            tryCloseReader(br);
            fd.close();
            
            Logger.debug(MessageFormat.format("Read {0} lines", lines.size()));
            
            for (int i = 0; i < lines.size(); i++)
            {
                Logger.debug(i + ") " + lines.get(i).Unfolded);
            }
        }
        catch (Exception x)
        {
            x.printStackTrace();
        }
        return lines;
    }
    
    public void toFile(String outFile) throws UnsupportedEncodingException, FileNotFoundException, IOException
    {
        File outDir = context.getDir(App.PATH_SENT, Context.MODE_WORLD_READABLE);
        File fout = new File(outDir.getAbsolutePath() + File.separator + outFile);
        BufferedWriter out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(fout),"UTF-8"));
        for (String s : header)
        {
            writeLine(out, s);
        }
        for (int i = 0; i < body.size(); i++)
        {
            if (checked[i])
            {
                for (String line : body.get(i).Original)
                {
                    writeLine(out, line);
                }
            }
        }
        for (String s : footer)
        {
            writeLine(out, s);
        }
        tryCloseWriter(out);
    }
    
    private void writeLine(BufferedWriter out, String line)
    {
        try
        {
            out.write(line);
            out.newLine();
        }
        catch (Exception x)
        {
            Logger.debug("Error writing line:");
            Logger.debug(line);
            x.printStackTrace();
        }
    }
    
    private void tryCloseWriter(BufferedWriter fs)
    {
        try
        {
            if (fs != null) fs.close();
        }
        catch ( IOException x)
        {
        }
    }
    
    private void tryCloseReader(BufferedReader fs)
    {
        try
        {
            if (fs != null) fs.close();
        }
        catch ( IOException x)
        {
        }
    }
}
