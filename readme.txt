This application helps you share your contacts as VCards (VCF) selectively, i.e., picking only the fields you want to share.

URL in Play store:
https://play.google.com/store/apps/details?id=com.timka.pickycontact